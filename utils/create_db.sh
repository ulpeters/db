#!/bin/bash -xe
# Create new DB in dockernized sql db

[ $# -eq 0 ] && { echo "Usage: $0 <database_name>"; exit 1; }

# Remove special characters and make db / username lower case
DB_NAME=`echo $1 | tr -dc '[:alnum:]\n\r' | tr '[:upper:]' '[:lower:]'`

# Source db root pass
source /opt/docker/db/.env

# Generate password for the new db
NEW_DB_PASS=`pwgen --ambiguous 32 1`

# Create db, corresponding user and grant privileges
docker exec db mysql -p$MYSQL_ROOT_PASSWORD --execute "CREATE DATABASE $DB_NAME; \
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, ALTER, DROP, INDEX ON $DB_NAME.* TO \"$DB_NAME\"@\"%\" IDENTIFIED BY \"$NEW_DB_PASS\"; \
FLUSH PRIVILEGES;"

# Create conf file to be source by other scripts
echo "DB_FOR=$1" >> $1__db.conf 
echo "DB_NAME=$DB_NAME" >> $1__db.conf
echo "DB_USER=$DB_NAME" >> $1__db.conf
echo "DB_PASS=$NEW_DB_PASS" >> $1__db.conf
