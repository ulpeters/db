#!/bin/bash -ex
# Create new DB in dockernized sql db

source /opt/docker/db/MYSQL_ROOT_PASSWORD.env

docker exec db mysql -p$MYSQL_ROOT_PASSWORD --execute "show databases;"
