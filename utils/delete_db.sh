#!/bin/bash -ex
# Create new DB in dockernized sql db

[ $# -eq 0 ] && { echo "Usage: $0 <database_name>"; exit 1; }

source /opt/docker/db/.env


docker exec db mysql -p$MYSQL_ROOT_PASSWORD --execute "DROP DATABASE $1;"
rm $1__db.conf
