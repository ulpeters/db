### mariadb

#### Setup
Setup the DB Root Password
   - `cp .env.template .env && vi .env`
   - The root password in `.env` gets stored in the DB during the intial container deployment.
     Changes of the file in a later stage have no effect.
   - The password can be changed later on only using `mysqladmin' -u root password 'new-password'`.


#### Maintenance
- **Create a new DB** by running `.utils/create_db.sh <database_name>`
  - `<database_name>` is the name and user of the new DB
  - The password is set randomly and stored with further details in `db_defintion__<database_name>.txt`.
- **List DBs** by running `./utils/db_names.sh`
- **Delete a DB** by running `.utils/delete_db.sh <database_name>`
- **For backup** see the container db_sqldump
- **Upgrade** `docker exec db sh -c 'mariadb-upgrade -p$MYSQL_ROOT_PASSWORD`
- **Restore** `docker exec db sh -c 'mysql -p$MYSQL_ROOT_PASSWORD < /mnt/backup/backupfile.sql'`
